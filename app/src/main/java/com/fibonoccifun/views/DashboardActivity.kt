package com.fibonoccifun.views

import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.fibonoccifun.R
import com.fibonoccifun.adapters.FibonacciAdapter
import com.fibonoccifun.listners.InfinitRecyclerOnScrollListener
import com.fibonoccifun.model.FiboModel
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.math.BigInteger
import java.util.*

class DashboardActivity : BaseActivity() {

    /*    As we have infinite loading of listitems for onscroll of user
          so we are here using the BigInteger instead Int or Double

    * If there are more input fields and validations in dashboard Activity, we can write the espresso test cases
    * Here are some helper methods for most common actions
    * ViewActions.click

    * ViewActions.typeText()

    * ViewActions.pressKey()

    * ViewActions.clearText()
    * Currently I have written unit test for recyclerview

 */

    private var mFibList: MutableList<FiboModel>? = null
    private var mLoadedItems = 0
    private var t1 = BigInteger("0");
    var t2 = BigInteger("1");

    private var mFibonacciAdapter: FibonacciAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        if (savedInstanceState != null) {
            //When rotation occurs
        } else {
            //when first time activity loaded
            initView()
        }

    }

    //Handle screen orientation to avoid making network calls
    override fun onSaveInstanceState(outState: Bundle?) {
        //  outState?.putCharSequenceArrayList("myData", mFibList)
        mFibList?.clear()
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        //mFibList = savedInstanceState?.getSerializable("myData") as MutableList<FiboModel>?

        //As we not storing the any data so lets call initiview itself
        initView()
    }

    private fun initView() {

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        dash_recyclerview.setLayoutManager(linearLayoutManager)
        mFibList = ArrayList()
        mFibonacciAdapter = FibonacciAdapter(this, mFibList as ArrayList)
        dash_recyclerview.adapter = mFibonacciAdapter
        addDataToList()

        dash_recyclerview.addOnScrollListener(object : InfinitRecyclerOnScrollListener() {
            override fun onLoadMore() {
                addDataToList()
            }
        })
    }

    fun updateAdp() {
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        dash_recyclerview.setLayoutManager(linearLayoutManager)
        mFibonacciAdapter = FibonacciAdapter(this, mFibList as ArrayList)
        dash_recyclerview.adapter = mFibonacciAdapter
        addDataToList()

        dash_recyclerview.addOnScrollListener(object : InfinitRecyclerOnScrollListener() {
            override fun onLoadMore() {
                addDataToList()
            }
        })
    }

    // Adds data to list with delay fo 1.5sec
    private fun addDataToList() {
        item_progress_bar.setVisibility(View.VISIBLE)
        Handler().postDelayed({

            //Load only 31 items at time to recyclerview
            for (i in 0..30) {
                mLoadedItems++
                var fiboModel = FiboModel(mLoadedItems.toString(), t1.toString())
                mFibList?.add(fiboModel)
                val sum = t1 + t2
                t1 = t2
                t2 = sum

            }
            mFibonacciAdapter?.notifyDataSetChanged()
            item_progress_bar.setVisibility(View.GONE)
        }, 1500)

    }

}

//Extension funtion to store serializable arraylist
private fun Bundle?.putCharSequenceArrayList(s: String, mFibList: MutableList<FiboModel>?) {

}
