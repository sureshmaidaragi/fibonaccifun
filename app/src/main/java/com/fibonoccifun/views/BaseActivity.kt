package com.fibonoccifun.views

import android.support.v7.app.AppCompatActivity

open class BaseActivity : AppCompatActivity()
/*
    * This application uses the Model view Presentator architecture
    * All the packages are uncoupled in way where we can write test cases without doing much changes on views, view model
    * We can write any functions which can access for all the child activity which inherits BaseActivity
 */