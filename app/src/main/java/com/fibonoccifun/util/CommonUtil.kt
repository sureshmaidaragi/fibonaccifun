package com.fibonoccifun.util

import android.content.Context
import android.net.ConnectivityManager

class CommonUtil {
    /*
        * This class can be used have common methods/functionality used through out the application lifecycle
        * Ex : Internet available is temporary no where used so far in project
     */
    companion object {

        /**
         * Checks for available internet connection
         */
        fun isInternetAvailable(ctx: Context): Boolean {
            val result = false
            val conMgr = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return if (conMgr.activeNetworkInfo != null) {
                conMgr.activeNetworkInfo.isConnected
            } else {

                result
            }
        }
    }
}