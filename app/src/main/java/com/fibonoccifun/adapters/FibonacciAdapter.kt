package com.fibonoccifun.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fibonoccifun.R
import com.fibonoccifun.model.FiboModel
import java.util.*

class FibonacciAdapter(mContext: Context, mStringList: ArrayList<FiboModel>) : RecyclerView.Adapter<FibonacciAdapter.ViewHolder>() {

    /*
    * This is the RecyclerView adapter class to present the data from model
    * Uses the viewholder patter to recycle the data instead of recreate for everytime
    */
    private var myList: List<FiboModel>? = null
    private var myContext: Context? = null

    init {
        this.myList = mStringList
        this.myContext = mContext
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fibonacci_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myList!!.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_cell.text = myContext?.resources?.getString(R.string.cell_lable) + " " + myList!!.get(position).cellTitle
        holder.tv_fib.text = myContext?.resources?.getString(R.string.fib_lable) + " " + myList!!.get(position).fibTitle

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_cell: TextView = itemView.findViewById(R.id.tv_cell) as TextView
        var tv_fib: TextView = itemView.findViewById(R.id.tv_fib) as TextView

    }
}