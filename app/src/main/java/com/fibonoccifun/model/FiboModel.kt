package com.fibonoccifun.model

import java.io.Serializable

data class FiboModel(
        val cellTitle: String,
        val fibTitle: String
) :Serializable
/*
* This is the kotlin data class used to hold the data from API response
* here we do not have any so just used to hold the cell value and fibonacci value
*
 */