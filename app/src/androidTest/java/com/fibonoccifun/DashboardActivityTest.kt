package com.fibonoccifun

import android.support.test.InstrumentationRegistry
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.fibonoccifun.views.DashboardActivity
import org.junit.*
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * If there are more input fields and validations in dashboard Activity, we can write the espresso test cases
 * Here are some helper methods for most common actions
 * ViewActions.click

 * ViewActions.typeText()

 * ViewActions.pressKey()

 * ViewActions.clearText()

 */
@RunWith(AndroidJUnit4::class)
class DashboardActivityTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.fibonoccifun", appContext.packageName)
    }

    private var mainActivity: DashboardActivity? = null

    @Rule
    val act = ActivityTestRule<DashboardActivity>(DashboardActivity::class.java)


    @Before
    @Throws(Exception::class)
    fun setUp() {
        mainActivity = act.getActivity()

    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        mainActivity!!.finish()

    }

    @Test
    @Throws(Exception::class)
    fun testOnCreate() {

//  Check weather the recyclerview is exist in activity_dashboard.xml layout.
        Assert.assertEquals("Initialised correctly!", (mainActivity!!.findViewById(R.id.dash_recyclerview) as RecyclerView))


    }
}
